# Intro
- This is a demo app for user to manage given invoice data.
- Frontend use Vue 2
- Backend use golang + gin
- DB use PostgreSQL

# Spec
Through the menu icon in the let-top corner of page, the user can access another pages.

## LineItems
- The user can browse through all the line-item data as a table, ordered by invoice id (ASC).
- When the table scrolled to the bottom, if there's still some data left in the DB, the app will fetch 20 invoice data; otherwise, there will be a notice at the bottom.
- When the user could edit line-item "adjustments" when this line-item isn't reviewed: click at the number and there will be an input filed, after focusout, the data will be updated to the server.The number of client side will be updated only when backend updated.
- Each line-item's billable amount will be calculated in the Subtotal column (sub-total = actuals +adjustments).
- On the top of page, user can the amount of grand-total (sum of each line-item's billableamount).
- The user can check individual line-items as "reviewed", and they can't edit adjustments anymore.
- User can filter the line-items by campaign name. The grand-total will be changed reactively.

## Campaigns
- User can see all the campaigns.
- User can see more detail of specific campaign by click the campaign name.

## Campaign
- There are line-items belongs to this sepecific campaign.
- User can see the sum of all sub-total of these line-items.

# ETC
for more detail of frontend and backend, please see the README inside these two folder.
