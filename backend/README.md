# Step1: Install dependencies 
- GoLang (runtime): https://golang.org/doc/install

- Docker (container): https://docs.docker.com/desktop/

- AIR (hot reload for golang): https://github.com/cosmtrek/air
```
go get -u github.com/cosmtrek/air
```
- CORS middleware for gin: https://github.com/gin-contrib/cors
```
go get github.com/gin-contrib/cors
```
- Migrate(database migration)
  
  Please see :https://github.com/golang-migrate/migrate/tree/master/cmd/migrate#installation for more detail

- Make(task runner)
  
  If you use windows OS, you can use [scoop](https://scoop.sh/) to install it.
  ```
  scoop install make
  ```

# Step2: Pull PostgreSQL image
```
// choose this version just because of its small size
docker pull postgres:13-alpine 
```

# Step3: create config files
Edit the config files to fit your need.
Please notice that the DB related setting need to be matched.

## 3-1: create config.yml
use config.example.yml as a template.

## 3-2: Edit docker-compose.yml

# Step4: Start the Container
```
docker-compose up
```
> since we need docker run container for us, please keep this terminal tab.

# Step5: Init DB with given data
Open another terminal tab, type this command to import data from json to DB in container.
```
make init
```
> when the importing is over, there will be a message on the terminal.

# Ready to develop
Now we have a DB in the container with imported data.
The environment is ready. We just need to keep the container running and use the command according to your usage:

- `start developing with hot-reload`
```
make dev
```
> This command will watch the whole backend folder and will build, rebuild and run again.


- `start web app`
```
make start_app
```
> This command only run the app. Won't rebuild and run after you editing file.

# Other Tools
## DB management
I use [TablePlus](https://tableplus.com/) personally.

They have both Mac and Win version.
