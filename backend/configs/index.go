package configs

import (
	"backend/utils"
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	DB struct {
		DbName   string `yaml:"db_name"`
		UserName string `yaml:"user_name"`
		Password string `yaml:"password"`
		Host     string `yaml:"host"`
		Port     int    `yaml:"port"`
	} `yaml:"db"`
}

var c Config

func Get() *Config {
	return &c
}

func Init() *Config {
	file, err := os.Open("./config.yml")
	if err != nil {
		utils.Log(err)
		panic("no config.yml")
	}
	defer file.Close()

	d := yaml.NewDecoder(file)
	if err := d.Decode(&c); err != nil {
		utils.Log(err)
		panic("config.yml format error")
	}

	return &c
}
