package db

import (
	"backend/utils"
	"database/sql"
	"fmt"
)

type Config struct {
	Driver   string
	Host     string
	DBName   string
	UserName string
	Password string
	Port     int
}

const (
	DriverPostgreSQL string = "postgreSQL"
)

func Get(dbConfig *Config) *sql.DB {
	switch dbConfig.Driver {
	case DriverPostgreSQL:
		return createPostgreSQLDB(dbConfig)
	default:
		panic("not supported driver")
	}
}

func createPostgreSQLDB(c *Config) *sql.DB {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		c.Host, c.Port, c.UserName, c.Password, c.DBName)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}

	err = db.Ping()
	if err != nil {
		panic(err)
	}

	utils.Log("Successfully connected!")
	return db
}
