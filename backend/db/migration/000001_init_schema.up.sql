CREATE TABLE "invoices"
(
    "id"             serial PRIMARY KEY,
    "campaign_id"    bigint             NOT NULL,
    "campaign_name" varchar            NOT NULL,
    "line_item_name" varchar            NOT NULL,
    "booked_amount"  decimal            NOT NULL,
    "actual_amount"  decimal            NOT NULL,
    "adjustments"    decimal            NOT NULL,
    "is_reviewed"    boolean            NOT NULL,
    "is_archived"    boolean            NOT NULL
);

CREATE TABLE "users"
(
    "id"       serial PRIMARY KEY,
    "name"     varchar            NOT NULL,
    "email"    varchar            NOT NULL,
    "password" varchar            NOT NULL
);

CREATE TABLE "invoice_comments"
(
    "id"         serial PRIMARY KEY,
    "invoice_id" bigint             NOT NULL,
    "user_id"    bigint             NOT NULL,
    "comment"    varchar            NOT NULL,
    "created_at" time               NOT NULL
);

ALTER TABLE "invoice_comments"
    ADD FOREIGN KEY ("invoice_id") REFERENCES "invoices" ("id");

ALTER TABLE "invoice_comments"
    ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");
