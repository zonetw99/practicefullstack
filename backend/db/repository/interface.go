package repository

import "backend/model"

type IRepository interface {
	GetInvoices(offset int, limit int) ([]model.Invoice, error)
	GetInvoicesByCampaignID(id int) ([]model.Invoice, error)
	UpdateInvoice(invoice *model.Invoice) error
	GetCampaigns(offset int, limit int) ([]model.Campaign, error)
}
