package postgresql

import (
	"backend/model"
	"fmt"
)

func (r *Repository) GetCampaigns(offset int, limit int) ([]model.Campaign, error) {
	query := fmt.Sprintf(`SELECT DISTINCT campaign_id, campaign_name FROM invoices ORDER by campaign_id ASC OFFSET %v limit %v`, offset, limit)
	return r.getCampaignFromDB(query)
}

func (r *Repository) getCampaignFromDB(query string) ([]model.Campaign, error) {
	rows, err := r.db.Query(query)
	defer rows.Close()
	if err != nil {
		return []model.Campaign{}, nil
	}

	var campaigns []model.Campaign
	for rows.Next() {
		var campaign model.Campaign
		if err := rows.Scan(
			&campaign.ID,
			&campaign.Name,
		); err != nil {
			return nil, err
		}
		campaigns = append(campaigns, campaign)
	}

	return campaigns, nil
}
