package postgresql

import (
	"database/sql"

	_ "github.com/lib/pq"

	"backend/db/repository"
)

type Repository struct {
	db *sql.DB
}

func New(db *sql.DB) repository.IRepository {
	return &Repository{
		db: db,
	}
}
