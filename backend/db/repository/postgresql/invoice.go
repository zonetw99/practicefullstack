package postgresql

import (
	"backend/model"
	"fmt"
)

func (r *Repository) UpdateInvoice(invoice *model.Invoice) error {
	stmt, err := r.db.Query(`
		UPDATE invoices
		SET adjustments=$1, is_reviewed=$2, is_archived=$3
		WHERE id=$4
		`, invoice.Adjustments, invoice.IsReviewed, invoice.IsArchived, invoice.ID)
	defer stmt.Close()
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	return nil
}

func (r *Repository) GetInvoices(offset int, limit int) ([]model.Invoice, error) {
	if offset < 0 {
		offset = 0
	}
	if limit > 20 {
		limit = 20
	}

	query := fmt.Sprintf(`SELECT * from invoices ORDER BY id LIMIT %v OFFSET %v`, limit, offset)
	return r.getInvoicesFromDB(query)
}

func (r *Repository) GetInvoicesByCampaignID(campaignID int) ([]model.Invoice, error) {
	if campaignID < 0 {
		campaignID = 0
	}
	query := fmt.Sprintf(`SELECT * from invoices WHERE campaign_id = %v`, campaignID)
	return r.getInvoicesFromDB(query)
}

func (r *Repository) getInvoicesFromDB(query string) ([]model.Invoice, error) {
	rows, err := r.db.Query(query)
	defer rows.Close()
	if err != nil {
		return []model.Invoice{}, nil
	}

	var invoices []model.Invoice
	for rows.Next() {
		var invoice model.Invoice
		if err := rows.Scan(
			&invoice.ID,
			&invoice.CampaignID,
			&invoice.CampaignName,
			&invoice.LineItemName,
			&invoice.BookedAmount,
			&invoice.ActualAmount,
			&invoice.Adjustments,
			&invoice.IsReviewed,
			&invoice.IsArchived,
		); err != nil {
			return nil, err
		}
		invoices = append(invoices, invoice)
	}

	return invoices, nil
}
