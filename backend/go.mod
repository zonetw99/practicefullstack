module backend

go 1.13

require (
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/lib/pq v1.10.0
	gopkg.in/yaml.v2 v2.2.8
)
