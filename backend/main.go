package main

import (
	"backend/model"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"

	"backend/configs"
	"backend/db"
	"backend/db/repository/postgresql"
)

func main() {
	c := configs.Init()
	db := db.Get(&db.Config{
		Driver:   db.DriverPostgreSQL,
		DBName:   c.DB.DbName,
		UserName: c.DB.UserName,
		Password: c.DB.Password,
		Host:     c.DB.Host,
		Port:     c.DB.Port,
	})
	repo := postgresql.New(db)
	r := gin.Default()
	r.Use(cors.Default())

	r.GET("/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"message": "Pong"})
	})

	r.GET("/invoice/all", func(c *gin.Context) {
		var invoices []model.Invoice
		var err error
		var offset, limit int

		if offset, err = strconv.Atoi(c.DefaultQuery("offset", "0")); err != nil {
			offset = 0
		}

		if limit, err = strconv.Atoi(c.DefaultQuery("limit", "20")); err != nil {
			limit = 20
		}

		if invoices, err = repo.GetInvoices(offset, limit); err != nil {
			c.JSON(http.StatusBadRequest, err.Error())
			return
		}
		c.JSON(http.StatusOK, gin.H{"invoices": invoices})
	})

	r.GET("/invoice/campaign/:campaignID", func(c *gin.Context) {
		var invoices []model.Invoice
		var err error
		var campaignID int

		if campaignID, err = strconv.Atoi(c.Param("campaignID")); err != nil {
			campaignID = 0
		}

		if invoices, err = repo.GetInvoicesByCampaignID(campaignID); err != nil {
			c.JSON(http.StatusBadRequest, err.Error())
			return
		}
		c.JSON(http.StatusOK, gin.H{"invoices": invoices})
	})

	r.POST("/invoice", func(c *gin.Context) {
		var invoice model.Invoice
		if err := c.ShouldBindJSON(&invoice); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"msg": "invoice format error"})
			return
		}
		if err := repo.UpdateInvoice(&invoice); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"msg": "db error"})
			return
		}
		c.JSON(http.StatusOK, gin.H{"msg": "db updated"})
	})

	r.GET("/campaign/all", func(c *gin.Context) {
		var err error
		var offset, limit int
		var campaigns []model.Campaign

		if offset, err = strconv.Atoi(c.DefaultQuery("offset", "0")); err != nil {
			offset = 0
		}

		if limit, err = strconv.Atoi(c.DefaultQuery("limit", "20")); err != nil {
			limit = 20
		}

		if campaigns, err = repo.GetCampaigns(offset, limit); err != nil {
			c.JSON(http.StatusBadRequest, err.Error())
			return
		}
		c.JSON(http.StatusOK, gin.H{"campaigns": campaigns})
	})

	r.Run(":1234")
	fmt.Println("Start server")
}
