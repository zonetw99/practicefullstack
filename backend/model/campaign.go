package model

type Campaign struct {
	ID   int    `json:"campaign_id"`
	Name string `json:"campaign_name"`
}
