package model

type Invoice struct {
	ID           int     `json:"id"`
	CampaignID   int     `json:"campaign_id"`
	CampaignName string  `json:"campaign_name"`
	LineItemName string  `json:"line_item_name"`
	BookedAmount float64 `json:"booked_amount"`
	ActualAmount float64 `json:"actual_amount"`
	Adjustments  float64 `json:"adjustments"`
	IsReviewed   bool    `json:"is_reviewed"`
	IsArchived   bool    `json:"is_archived"`
}
