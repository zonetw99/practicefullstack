module zonetw.dev

go 1.13

require (
	github.com/lib/pq v1.10.0 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
