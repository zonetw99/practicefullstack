package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	_ "github.com/lib/pq"
	"gopkg.in/yaml.v2"
)

type Config struct {
	DB struct {
		DbName   string `yaml:"db_name"`
		UserName string `yaml:"user_name"`
		Password string `yaml:"password"`
		Host     string `yaml:"host"`
		Port     int    `yaml:"port"`
	} `yaml:"db"`
}

type Invoice struct {
	ID           int     `json:"id"`
	CampaignID   int     `json:"campaign_id"`
	CampaignName string  `json:"campaign_name"`
	LineItemName string  `json:"line_item_name"`
	BookedAmount float64 `json:"booked_amount"`
	ActualAmount float64 `json:"actual_amount"`
	Adjustments  float64 `json:"adjustments"`
}

var c Config
var db *sql.DB

func main() {
	readDBConfig()
	db = connectDB(c.DB.DbName, c.DB.UserName, c.DB.Password, c.DB.Host, c.DB.Port)
	defer db.Close()

	var invoices []Invoice
	var err error
	if invoices, err = getInvoiceDataFromJson(); err != nil {
		return
	}
	writeDataIntoDB(invoices)
}

func writeDataIntoDB(invoices []Invoice) {
	for _, invoice := range invoices {
		stmt, err := db.Prepare(`
			INSERT INTO invoices(
			 campaign_id,
			 campaign_name,
			 line_item_name,
			 booked_amount,
			 actual_amount,
		     adjustments,
			 is_reviewed,
			 is_archived
		 ) VALUES ($1,$2,$3,$4,$5,$6,$7,$8)`)
		if err != nil {
			log("sql syntax err: " + err.Error())
			return
		}
		_, err = stmt.Exec(
			invoice.CampaignID,
			invoice.CampaignName,
			invoice.LineItemName,
			invoice.BookedAmount,
			invoice.ActualAmount,
			invoice.Adjustments,
			false,
			false,
		)
		if err != nil {
			log(err)
			return
		}
		defer stmt.Close()
	}
	log("data imported")
	os.Exit(0)
}

func connectDB(dbName string, userName string, password string, host string, port int) *sql.DB {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, userName, password, dbName)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}

	err = db.Ping()
	if err != nil {
		panic(err)
	}

	log("db connected!")

	return db
}

func getInvoiceDataFromJson() ([]Invoice, error) {
	jsonFIle, err := os.Open("placements_teaser_data.json")
	if err != nil {
		log(err.Error())
		return nil, err
	}
	defer jsonFIle.Close()
	log("invoice json loaded")

	var invoices []Invoice
	byteVal, _ := ioutil.ReadAll(jsonFIle)
	json.Unmarshal(byteVal, &invoices)
	log("invoice json parsed")
	return invoices, nil
}

func readDBConfig() {
	file, err := os.Open("./config.yml")
	if err != nil {
		log(err)
		return
	}
	defer file.Close()

	d := yaml.NewDecoder(file)
	if err := d.Decode(&c); err != nil {
		log(err)
		return
	}
}

// helper
func log(msg interface{}) {
	fmt.Println(msg)
}
