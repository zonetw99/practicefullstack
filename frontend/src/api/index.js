import axios from 'axios'

const BASE_URL = '//localhost:1234'

const newAxios = axios.create({
  baseURL: BASE_URL
})

function getInvoice (offset, limit) {
  return newAxios.get(`/invoice/all?offset=${offset}&limit=${limit}`)
}
function getInvoiceByCampaignID (id) {
  return newAxios.get(`/invoice/campaign/${id}`)
}
function getCampaigns (offset, limit) {
  return newAxios.get(`/campaign/all?offset=${offset}&limit=${limit}`)
}
function updateInvoice (invoice) {
  return newAxios.post('/invoice', invoice)
}
const api = {
  getInvoice,
  getInvoiceByCampaignID,
  getCampaigns,
  updateInvoice
}

export default api
