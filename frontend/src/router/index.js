import Vue from 'vue'
import VueRouter from 'vue-router'
import Campaigns from '../views/Campaigns.vue'
import LineItems from '@/views/LineItems'

Vue.use(VueRouter)

const routes = [
  {
    path: '/campaigns',
    name: 'Campaigns',
    component: Campaigns
  },
  {
    path: '/campaign/:id',
    name: 'Campaign',
    component: LineItems
  },
  {
    path: '/line_items',
    name: 'LineItems',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/LineItems.vue')
  },
  {
    path: '*',
    name: 'Info',
    component: () => import('../views/Info')
  }
]

const router = new VueRouter({
  routes
})

export default router
