function compose (...functionArray) {
  const check = !functionArray.some(func => typeof func !== 'function')

  if (check) {
    return function (input) {
      return functionArray.reduce((sum, func) => {
        return func(sum)
      }, input)
    }
  }
  throw new Error('plz func')
}

export default {
  compose
}
