import fp from '@/utils/fp'

function convertInvoicesToLineItems (invoices) {
  return invoices.map(invoice => {
    return {
      ...invoice,
      initAdjustments: invoice.adjustments,
      isEditing: false
    }
  })
}

function filterInvoiceByCampaignName (param) {
  return param.invoices.filter(invoice => invoice.campaign_name.indexOf(param.keyword) > -1)
}

const getLineItemsByCampaignName = fp.compose(filterInvoiceByCampaignName, convertInvoicesToLineItems)

export default {
  convertInvoicesToLineItems,
  filterInvoiceByCampaignName,
  getLineItemsByCampaignName
}
