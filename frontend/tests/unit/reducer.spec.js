import reducer from '@/utils/reducer'

const invoices = [
  { id: 1, campaign_name: 'A', campaign_id: 1, line_item_name: 'line1', booked_amount: 1, actual_amount: 1, adjustments: 0, is_reviewed: false, is_archived: false }
]

describe('test convertInvoicesToLineItems', () => {
  it('should add data to lineItem', () => {
    const result = reducer.convertInvoicesToLineItems(invoices)

    expect(result.length).toEqual(1)
    expect(result[0].initAdjustments).toEqual(result[0].adjustments)
    expect(result[0].isEditing).toEqual(false)
  })
})

describe('test filterInvoiceByCampaignName', () => {
  it('should filter invoices whose campaign name has keyword', () => {
    const result = reducer.filterInvoiceByCampaignName({ invoices, keyword: 'A' })
    expect(result.length).toEqual(1)
  })
  it('should filter invoices whose campaign name does not have keyword', () => {
    const result = reducer.filterInvoiceByCampaignName({ invoices, keyword: 'B' })
    expect(result.length).toEqual(0)
  })
})

describe('test getLineItemsByCampaignName', () => {
  it('should get filtered lineItems whose campaign name has keyword and have been initialized', () => {
    const lineItems = reducer.getLineItemsByCampaignName({ invoices, keyword: 'A' })

    expect(lineItems.length).toEqual(1)
    expect(lineItems[0].initAdjustments).toEqual(lineItems[0].adjustments)
    expect(lineItems[0].isEditing).toEqual(false)
  })
  it('should filter invoices whose campaign name does not have keyword', () => {
    const lineItems = reducer.getLineItemsByCampaignName({ invoices, keyword: 'B' })
    expect(lineItems.length).toEqual(0)
  })
})
